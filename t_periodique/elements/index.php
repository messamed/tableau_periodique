<?php 
require_once("../connect.php");

$id=$_GET['id'];
$sql="SELECT * FROM elements WHERE num_atomique='".$id."' ";
$query=mysqli_query($periodic,$sql);

$element=mysqli_fetch_assoc($query);


 ?>
<html>

<head>

<title><?php echo  ucfirst($element['nom']) ?> (<?php echo  $element['symbole'] ?>) -Tableau periodique </title>

<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<meta name="description" content="Hydrogen is a chemical element with chemical symbol H and atomic number 1. With an atomic weight of 1.00794 u, hydrogen is the lightest element on the periodic table.">

<meta property="og:title" content="Hydrogen (H) - The Periodic Table Of Elements">


</head>
<body class="">
<div id="header">
<img src="../images/element/<?php echo $id ?>.jpg" width="200" height="130" id="element-img">
<img src="../images/position/<?php echo $id ?>.jpg" width="170" height="100" id="position-img" >

</div>
<div class="wrapper">
<div class="wrapper-inner">
<div class="card summary">
<div class="element other-nonmetals">
<!--div class="atomic-orbital-shells">
<span class="K">1</span>
<span class="L"></span>
<span class="M"></span>
<span class="N"></span>
<span class="O"></span>
<span class="P"></span>
<span class="Q"></span>
</div-->
<div class="atomic-number"> <?php echo $element['num_atomique'] ?>
</div>
<div class="element-symbol">
<strong><?php echo $element['symbole'] ?></strong>
</div>
<div class="element-name">
<?php echo $element['nom'] ?>
</div>
<div class="atomic-mass">
<?php echo $element['mass_atomique'] ?></div>
</div>
<ul>
<li>Symbol: <sub>1</sub>H</li>
<li>Nom : <?php echo $element['nom'] ?></li>
<li>Numero atomique: <?php echo $element['num_atomique'] ?></li>
<li> Masse atomique: <?php echo $element['mass_atomique'] ?> u</li>
<li> configuration d'electron : <?php echo $element['e_config'] ?></li>
<li>Electronegativité: <?php echo $element['e_negativite'] ?></li>
<li> Energie d'ionisation   : <?php echo $element['energie_ionisation'] ?></li>
<li> les états d'oxydation   : <?php echo $element['etats_oxydation'] ?></li>
<li> Rayon atomique   : <?php echo $element['rayon_atomique'] ?></li>
<li> Rayon de vanderwaals   : <?php echo $element['rayon_vanderwaals'] ?></li>


</ul>
</div>
<div class="card description">
<p><?php echo  ucfirst($element['description']) ?></p>
</div>
<div class="card overview">
<p><strong> propriétés générales</strong></p>
<ul>
<li>Groupe: groupe  <?php echo $element['groupe'] ?></li>
<li>periode: periode <?php echo $element['periode'] ?></li>
<li>Famille :   <?php echo $element['famille'] ?></li>
<li> état standard  : <?php echo $element['etat_standard'] ?></li>


</ul>
<p><strong>propriétés physique</strong></p>
<ul>

<li>Couleur: <span style=' width:20px; height:10px; background-color:#<?php echo $element['couleur_cpk'] ?>'>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span></li>
<li> point de Vaporisation : <?php echo $element['point_vaporisation']-273 ?> °C</li>
<li> point de fusion : <?php echo $element['point_fusion']-273 ?> °C</li>
<li> Densité : <?php echo $element['densite'] ?> (g cm<sup>−3</sup>)</li>
<li> Volume : <?php echo $element['volume'] ?> m<sup>3</sup></li>
<li> conductivité : <?php echo $element['conductivite'] ?> mm<sup>3</sup></li>
<li> chaleur : <?php echo $element['chaleur']-273 ?> °C </li>
<li> chaleur de Vaporisation : <?php echo $element['chaleur_evaporation']-273 ?> °C</li>
<li> chaleur de fusion : <?php echo $element['chaleur_fusion']-273 ?> °C</li>
<li> type de  laison   : <?php echo $element['type_liaison'] ?></li>

</ul>
<p><strong>histoire</strong></p>
<ul>
<li>année de découverte  :<?php echo $element['annee_decouverte'] ?></li>
<li>découvreur(s)  :<?php echo $element['decouvreur'] ?></li>

</ul>
</div>
</div>
</div>

<script type="text/javascript">
        jQuery('.close').on('click', function(){

            jQuery('.info').slideUp(200);
            jQuery('.group').removeClass('focus-group');

        });        
    </script>
<style>
#header{ height:150px}
#element-img{ margin:10px 0  0 40px}
#position-img{ float:right; margin:20px 40px}
    body {
        margin: 0px;
        width: 100%;
        background: white;
        font-family: Arial;
    }

    html {
        width: 100%;
    }

    a {
	    transition: .7s;
	    color: #03a3ea;
	}

	a:hover {
	    color: #949494;
	}

    .no-focus .card .element {
        border: 3px solid #a9a9a9;
    }

    .no-focus .card div {
        color: darkgray
    }

    .no-focus .card {
        color: darkgray;
    }

    .wrapper-inner {
        display: flex;
        justify-content: space-around;
        flex-direction: row;
        border: 2px solid #e2e2e2;
        background: whitesmoke;
        padding: 20px;
    }

    .card {
        flex: 1 1;
        max-width: 20%;
        min-width: 350px;
        box-sizing: border-box;
        padding: 20px;
        background: white;
        border: 2px solid #e2e2e2;
        border-radius: 5px;
        position: relative;
        transition: .7s;
        font-size: 15px;
        color: black;
    }

    .other-nonmetals.focus-group:after {
        border-top: 10px solid #76FF03;
    }
    .card .other-nonmetals {
        border: 3px solid #76FF03;
    }

    .noble-gases.focus-group:after {
        border-top: 10px solid #df5ff5;
    }
    .card .noble-gases {
        border: 3px solid #df5ff5;
    }

    .halogens.focus-group:after {
        border-top: 10px solid #18FFFF;
    }
    .card .halogens {
        border: 3px solid #18FFFF;
    }

    .metalloids.focus-group:after {
        border-top: 10px solid #00E676;
    }
    .card .metalloids {
        border: 3px solid #00E676;
    }

    .alkaline-earth-metals.focus-group:after {
        border-top: 10px solid #eaea18;
    }
    .card .alkaline-earth-metals {
        border: 3px solid #eaea18;
    }

    .alkali-metals.focus-group:after {
        border-top: 10px solid #f7a91d;
    }
    .card .alkali-metals {
        border: 3px solid #f7a91d;
    }

    .transition-metals.focus-group:after {
        border-top: 10px solid #ff3e7f;
    }
    .card .transition-metals {
        border: 3px solid #ff3e7f;
    }

    .post-transition-metals.focus-group:after {
        border-top: 10px solid #40C4FF;
    }
    .card .post-transition-metals {
        border: 3px solid #40C4FF;
    }

    .lanthanoids.focus-group:after {
        border-top: 10px solid #fd6d3f;
    }
    .card .lanthanoids {
        border: 3px solid #fd6d3f;
    }

    .actinoids.focus-group:after {
        border-top: 10px solid #ff1744;
    }
    .card .actinoids {
        border: 3px solid #ff1744;
    }
    
    .card div {
        transition: .7s;
    }

    .card .element {
        background: transparent;
        box-sizing: border-box;
        padding: 20px;
        max-width: 250px;
        max-height: 250px;
        color: black;
    }

    .card .element-symbol {
        font-size: 100px;
        text-align: left;
        color: black;
    }

    .card .element-name {
        font-size: 30px;
        display: block;
        color: black;
    }

    .card .atomic-mass {
        display: block;
        font-size: 18px;
        max-width: none;
        margin-top: 10px;
        color: black;
    }

    .card .atomic-orbital-shells {
        font-size: 20px;
        right: 20px;
        display: block;
        color: black;
    }
    
    .card .atomic-orbital-shells span {
        line-height: normal;
        color: black;
    }

    .card .atomic-number {
        font-size: 30px;
        text-align: left;
        display: block;
        color: black;
    }

    .card p:first-of-type {
        margin-top: 0px;
    }

    .card.description p:first-of-type:first-letter {
        font-size: 50px;
        font-weight: bold;
        float: left;
        position: relative;
        line-height: 28px;
        margin-bottom: 5px;
    }

    .card ul {
        border-top: 1px solid #e2e2e2;
        padding-top: 20px;
    }

    .card ul:last-of-type {
        margin-bottom: 0px;
    }

    @media(max-width: 1200px) {

        .card {
            flex-wrap: wrap;
            min-width: 49%;
        }

        .card.overview {
            min-width: 100%;
            margin-top: 10px;
        }

        .card.summary {
            margin-right: 5px;
        }

        .card.description {
            margin-left: 5px;
        }

        .wrapper-inner {
            justify-content: space-between;
            flex-wrap: wrap;
        }

        .info .close {
            top: 5px;
            right: 5px;
        }

    }

    @media(max-width: 768px) {

        .card .element {
            position: relative;
            margin: auto;
        }

        .card .element-symbol {
            font-size: 70px;
        }

    }

    @media(max-width: 600px) {     

        .card.summary {
            min-width: 100%;
            margin: auto;
        }

        .card.description {
            min-width: 100%;
            margin-top: 10px;
            margin-left: 0px;
        }

        .card .element {
            min-height: 250px;
        }

    }

    @media(max-width: 400px) {

        .card .element {
            max-height: 150px;
            max-width: 150px;
            min-height: initial;
            padding: 10px;
        }

        .card .element-symbol {
            font-size: 40px;
            text-align: left;
        }

        .card .element-name {
            font-size: 18px;
            display: block;
        }

        .card .atomic-mass {
            display: block;
            font-size: 15px;
            max-width: none;
            margin-top: 10px;
        }

        .card .atomic-orbital-shells {
            font-size: 12px;
            right: 10px;
            display: block;
        }

        .card .atomic-number {
            font-size: 15px;
            text-align: left;
            display: block;
        }

        .card ul {
            padding-left: 20px;
        }

    }
    </style>
</body></html>