
Introduction
------------
periodic c'est une application web qui représente le tableau périodique d'une facon ulistré
 et décrit tous les caractéristique des élément chimique ainsi que leurs classification 

Prérequis
------------
PHP >=7
MySQL >= 5.7

Configuration
-------------

  * la base de données est associé dans le dossier "BD"

  * l'application est actuellement configuré à utiliser une base de donnée hébergé dans un cloud publique 

  * si vous souhitez conncter l'aaplication à votre base de donnée  veillez modifiier  l'utilisateur 
  et le mot de passe de la connexion dans le fichier "connect.php"

Utilisation
-------------

 * quand vous faites un hover sur l'élément une petite fenetre s'affiche pour donner un coup d'oeil sur l'élément et quand vous
  clickez sur un élément une onglet s'ouvrira pour afficher plus de détails sur l'élemennt chimique 

 * la liste des filtres en haut vous permettera de filtre les éléments chimique selon le groupe , la fammile et la période ..etc

 * le filtre de température vous permettera de connaitre l'état physique de chaque élément dans le tableau 
   à une certaine dégré de température.

 * si un filtre (effet css/javascript) ne marche pas veillez utiliser une version recente de navigateur (chrome ou firefox)



