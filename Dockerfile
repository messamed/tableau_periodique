FROM php:7.2-apache
COPY t_periodique/ /var/www/html/
EXPOSE 80
RUN ls -la /var/www/html/*
RUN docker-php-ext-install mysqli && docker-php-ext-enable mysqli
CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]


