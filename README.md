# tableau_periodique

 
** Pipeline **

* pipeline pour déployer une application web php sur un conteneur docker et la tester  
* cette pipeline est structuré en 4 jobs qui sont comme suite : 

BUILD
-------------

  * il s'agit de construire une images docker pour une application web définie dans le fichier Dockerfile 
  * faire le push de l'application vers le gitlab container registry 

Test
-------------

  * pour tester l'application on lance le conteneur depuis l'image qui à été deja sauvegardé dans le gitlab registry
  * on fait  un simple test par la commande curl por tester que l'application marche et à été bien publiée sur le port 80 

Lint
------------

  * on fait le lint du Dockerfile par l'outil hadolint pour bien valider le syntax 

Scan
------------
  
 * le dernier job sert à scanner les vulnérabilités du conteneur par l'outil Trivy


