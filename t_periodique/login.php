<?php
error_reporting(0); 
require 'connect.php';
session_start();
if (isset($_SESSION['user_id'])) {
    header("location:index.php");
}
ob_start(); 

?>
<!DOCTYPE html>
<html>
<head>
    <title>Social Network</title>
    <link rel="stylesheet" type="text/css" href="src/main.css?j=1">
    <style>
        .container{
            margin: 40px auto;
            width: 400px;
        }
        .content {
            padding: 30px;
            background-color: white;
            box-shadow: 0 0 5px #4267b2;
        }
    </style>
</head>
<body>
<br><br>
    <div class="container">
        <div class="tab">
               <button class="tablink active" onclick="openTab(event,'signin')" id="link1">Login</button>
            <button class="tablink" onclick="openTab(event,'signup')" id="link2">Créer compt</button>
          

        </div>
       
        <div class="content">
            <div class="tabcontent" id="signin" >
                <form method="post" onsubmit="return validateLogin()">
                    <label>Email<span>*</span></label><br>
                    <input type="text" name="useremail" id="loginuseremail">
                    <div class="required"></div>
                    <br>
                    <label>Password<span>*</span></label><br>
                    <input type="password" name="userpass" id="loginuserpass">
                    <div class="required"></div>
                    <br><br>
                    <input type="submit" value="Login" name="login">
                </form>
            </div>
                        <div class="tabcontent" id="signup" >
                <form method="post" onsubmit="return validateRegister()">
                    <!--Package One-->
                    <hr>
                    <!--First Name-->
                    <label>Nom<span>*</span></label><br>
                    <input type="text" name="userfirstname" id="userfirstname">
                    <div class="required"></div>
                    <br>
                    <!--Last Name-->
                    <label>Prénom<span>*</span></label><br>
                    <input type="text" name="userlastname" id="userlastname">
                    <div class="required"></div>
                    <br>
                    <!--Nickname-->
                   
                    <!--Password-->
                    <label>Mot de passe <span>*</span></label><br>
                    <input type="password" name="userpass" id="userpass">
                    <div class="required"></div>
                    <br>
                    <!--Confirm Password-->
                    <label>Confirmer le mot de passe<span>*</span></label><br>
                    <input type="password" name="userpassconfirm" id="userpassconfirm">
                    <div class="required"></div>
                    <br>
                    <!--Email-->
                    <label>Email<span>*</span></label><br>
                    <input type="text" name="useremail" id="useremail">
                    <div class="required"></div>
                    <br>
                    <!--Birth Date-->
                   Date de naissance<span>*</span><br>
                    <select name="selectday">
                    <?php
                    for($i=1; $i<=31; $i++){
                        echo '<option value="'. $i .'">'. $i .'</option>';
                    }
                    ?>
                    </select>
                    <select name="selectmonth">
                    <?php
                    echo '<option value="1">January</option>';
                    echo '<option value="2">February</option>';
                    echo '<option value="3">March</option>';
                    echo '<option value="4">April</option>';
                    echo '<option value="5">May</option>';
                    echo '<option value="6">June</option>';
                    echo '<option value="7">July</option>';
                    echo '<option value="8">August</option>';
                    echo '<option value="9">September</option>';
                    echo '<option value="10">October</option>';
                    echo '<option value="11">Novemeber</option>';
                    echo '<option value="12">December</option>';
                    ?>
                    </select>
                    <select name="selectyear">
                    <?php
                    for($i=2017; $i>=1900; $i--){
                        if($i == 1996){
                            echo '<option value="'. $i .'" selected>'. $i .'</option>';
                        }
                        echo '<option value="'. $i .'">'. $i .'</option>';
                    }
                    ?>
                    </select>
                   
                  <br><br>
                    <!--Package Two-->
                    <hr>
                    <!--Marital Status-->
                  
                    <input type="submit" value="Créer" name="register">
                </form>
            </div>

        </div>
      
    </div>
    <script src="src/main.js"></script>
</body>
</html>

<?php

if ($_SERVER['REQUEST_METHOD'] == 'POST') { // A form is posted
    if (isset($_POST['login'])) { // Login process
        $useremail = $_POST['useremail'];
        $userpass = $_POST['userpass'];

        $query = mysqli_query($periodic, "SELECT * FROM users   WHERE email = '$useremail' AND password = '$userpass'");
        if($query){
            if(mysqli_num_rows($query) == 1) {
                $row = mysqli_fetch_assoc($query);
                $_SESSION['user_id'] = $row['id'];
			    $_SESSION['username'] = $row['nom']." ". $row['prenom'];
				$_SESSION['is_admin'] =  ($row['previlege']==0) ? false : true;

                header("location:index.php");
            }
            else {
                ?> <script>
                    document.getElementsByClassName("required")[0].innerHTML = "Invalid Login Credentials.";
                    document.getElementsByClassName("required")[1].innerHTML = "Invalid Login Credentials.";
                </script> <?php
            }
        } else{
            echo mysqli_error($periodic);
        }
    }
    if (isset($_POST['register'])) { // Register process
        // Retrieve Data
        $userfirstname = $_POST['userfirstname'];
        $userlastname = $_POST['userlastname'];
        $userpassword = $_POST['userpass'];
        $useremail = $_POST['useremail'];
        $userbirthdate = $_POST['selectyear'] . '-' . $_POST['selectmonth'] . '-' . $_POST['selectday'];

        /* Check for Some Unique Constraints
        $query = mysqli_query($periodic, "SELECT  email FROM users WHERE  email = '$useremail'");
        if(mysqli_num_rows($query) > 0){
            $row = mysqli_fetch_assoc($query);
            
            if($useremail == $row['email']){
                ?> <script>
                document.getElementsByClassName("required")[7].innerHTML = "This Email already exists.";
                </script> <?php
            }
        }
		*/
        // Insert Data
        $sql = "INSERT INTO users(nom, prenom, password,b_day, email)
                VALUES ('$userfirstname', '$userlastname', '$userpassword', '$userbirthdate', '$useremail')";
        $query = mysqli_query($periodic, $sql);
        if($query){
            $query = mysqli_query($periodic, "SELECT id FROM users WHERE email = '$useremail'");
            $row = mysqli_fetch_assoc($query);
            //$_SESSION['user_id'] = $row['id'];
			// $_SESSION['username'] = $row['nom']." ". $row['prenom'];
            header("location:index.php");
        }
    }
}
?>
