
<?php
error_reporting(0);
require_once("connect.php");
session_start();

$sql="SELECT num_atomique,nom,symbole,mass_atomique,e_config,energie_ionisation,etats_oxydation,densite,point_fusion,point_vaporisation,famille,groupe,periode,is_metal FROM elements  ";
$query=mysqli_query($periodic,$sql);

while($row=mysqli_fetch_assoc($query))
{
	$elements[$row['num_atomique']]=$row;
//	echo json_encode($elements);
	
	}
	
	function  set_attr($element){
     $ret= "num_atomique='".$element[num_atomique]."' ";
	 $ret.= "nom='".$element[nom]."' ";
	  $ret.= "symbole='".$element[symbole]."' ";
	  $ret.= "mass_atomique='".$element[mass_atomique]."' ";
	  $ret.= "e_config='".$element[e_config]."' ";
	  $ret.= "energie_ionisation='".$element[energie_ionisation]."' ";
	  $ret.= "densite='".$element[densite]."' ";
	  $ret.= "etats_oxydation='".$element[etats_oxydation]."' ";
     $ret.= "groupe='".$element[groupe]."' ";
	  $ret.= "periode='".$element[periode]."' ";
	  $ret.= "famille='".$element[famille]."' ";
	  $ret.= "metal='".$element[is_metal]."' ";

	  return $ret;
	}
 ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Tableau Periodique</title>
<link href="src/style.css?i=4" rel="stylesheet" type="text/css" >

<script src="src/jquery.js" type="text/javascript"></script>
<script>
$(document).ready(function(){
	  var tempClicked=0;

$(".real_cell").hover(function(){

//alert($(this).children(".element").attr("id"));
var color=$(this).children(".element").css("background-color");
var num_atomique=$(this).children(".element").attr("num_atomique");
var nom=$(this).children(".element").attr("nom");
var symbole=$(this).children(".element").attr("symbole");
var densite=$(this).children(".element").attr("densite");
var etats_oxydation=$(this).children(".element").attr("etats_oxydation");
var e_config=$(this).children(".element").attr("e_config");
var energie_ionisation=$(this).children(".element").attr("energie_ionisation");
var mass_atomique=$(this).children(".element").attr("mass_atomique");
				   $('.trbox_he').empty();
				   $('.element_detail').empty();
				   $("#nom").empty();



				   $("#e_config").append(e_config);
				   $("#nom").append(nom);
				   $("#symbole").append(symbole);
				   $("#densite").append(densite);
				   $("#etats_oxydation").append(etats_oxydation);
				   $("#energie_ionisation").append(energie_ionisation);
				   $("#mass_atomique").append(mass_atomique);
				   $("#num_atomique").append(num_atomique);

				   $("#murrayImgId").attr("src","images/element/"+num_atomique+".jpg");
				   
                   $(".element_header").css("background-color", color);
                   $(".element_hover_details").css("background-color", color);
				   $(".element_hover_details").css("margin-top", "-102px");

                   $(".td_height").css("border-bottom", "1px solid "+color );
                   $(".td_height").css("border-left",  "1px solid "+color);
				   
				    $(".tempPopup_blk").css("display",  "none");
                   $("#popup_container").css("display",  "block");


;
} ,
    function () {
                   $("#popup_container").css("display",  "none");
				if(tempClicked==1)   $(".tempPopup_blk").css("display",  "block");

    });
	
	    var elementsData = <?php echo json_encode($elements); ?>;
	  $("#temp_plus").click(function(e) {
	var val=$('#rang').val();
	   val= parseInt(val)+1;
	$('#rang').val(val);
	$('#kelvin').text(val);
	$('#Celsius').text(val-273);
      $(".tempPopup_blk").css("display",  "block");
	  tempClicked=1;


			     change_cell_color(val,elementsData);

	  });
	   $("#temp-").click(function(e) {
	 var val=$('#rang').val();
	   val= parseInt(val)-1;
	    $('#rang').val(val);
		$('#kelvin').text(val);
		$('#Celsius').text(val-273);
      $(".tempPopup_blk").css("display",  "block");
	  tempClicked=1;

		     change_cell_color(val,elementsData);

	  })
	   $("#rang").change(function(e) {
	     var val=parseInt($('#rang').val());
		 	$('#kelvin').text(val);
			$('#Celsius').text(val-273);
           $(".tempPopup_blk").css("display",  "block");
	       tempClicked=1;

		     change_cell_color(val,elementsData);
	   })

   
   function change_cell_color(temperature,array){
   for (var i = 1; i < 119; i++) {
	  var element="#element_"+i;
	  var point_fusion =array[i]['point_fusion'];
	  var point_vaporisation=array[i]['point_vaporisation'];
	  
	  if(temperature <= point_fusion && (point_vaporisation!=0 || point_fusion!=0) ){
		 $(element).css("background",  " #B72326 ");
		  }else if(temperature <= point_vaporisation && (point_vaporisation!=0 || point_fusion!=0) ){
		 $(element).css("background",  "  blue ");
			  }else if( point_vaporisation!=0 ){
		 $(element).css("background",  "green ");
				  }
				  else{
		 $(element).css("background",  "  grey");
				  }

}
   
   }
   
   	  $(".filter_g").click(function(e) {
		  $(".element").css("opacity","1") ;
		   $(".element").css("border","1px solid rgba(0,0,0,0.05)")
		var groupe=$(this).attr("id").split('_');
		for(var i=1;i<119; i++){
			if(elementsData[i]['groupe']!=groupe[1]) 
			{$("#element_"+i).css("opacity","0.2") ;
			} else $("#element_"+i).css("border","1px solid red") ;

			}
  
	  })
      $(".filter_p").click(function(e) {
			  $(".element").css("opacity","1") ;
		   $(".element").css("border","1px solid rgba(0,0,0,0.05)")
		var periode=$(this).attr("id").split('_');
		for(var i=1;i<119; i++){
			if(elementsData[i]['periode']!=periode[1]) 
			{$("#element_"+i).css("opacity","0.2") ;
			} else $("#element_"+i).css("border","1px solid red") ;

			}
  
	  })

      $(".filter_f").click(function(e) {
			  $(".element").css("opacity","1") ;
		   $(".element").css("border","1px solid rgba(0,0,0,0.05)")
		var famille=$(this).attr("id");
		for(var i=1;i<119; i++){
			if(elementsData[i]['famille']!=famille) 
			{$("#element_"+i).css("opacity","0.2") ;
			} else $("#element_"+i).css("border","1px solid red") ;

			}
  
	  })
	  
      $(".filter_metal").click(function(e) {
			  $(".element").css("opacity","1") ;
		   $(".element").css("border","1px solid rgba(0,0,0,0.05)")
		var is_metal=parseInt( $(this).attr("id"));
		for(var i=1;i<119; i++){
			if(elementsData[i]['is_metal']!=is_metal) 
			{$("#element_"+i).css("opacity","0.2") ;
			} else $("#element_"+i).css("border","1px solid red") ;

			}
  
	  })

      $("#densite-filter").click(function(e) {
			  $(".tempPopup_blk").css("display",  "none");			  
	       tempClicked=0;
			  $(".element").css("opacity","1") ;
		   $(".element").css("border","1px solid rgba(0,0,0,0.05)");
		   
		for(var i=1;i<119; i++){
			var densite=elementsData[i]['densite'] ;
			var pers=(densite/10)*100;
			var rest=100-pers;
			
				$("#element_"+i).css("background","linear-gradient(180deg, grey "+rest+"%, #00FFFF "+pers+"%)") ;
			
			
		}
  

				  });
	  $("#reset-filter").click(function(e) {
				 $(".tempPopup_blk").css("display",  "none");	
				  tempClicked=0;
			  $(".element").css("opacity","1") ;
		     $(".element").css("border","1px solid rgba(0,0,0,0.05)");
			 
		  $(".element").css("background","rgba(0, 128, 128, 0.6)");
		 $(".métaux_alcalins").children(".element").css("background","#40C4FF ");
		 $(".métaux_alcalino-terreux").children(".element").css("background","rgba(0, 128, 128, 0.9) ");
		 $(".lanthanides").children(".element").css("background","#fd6d3f ");
		 $(".actinides").children(".element").css("background","#eaea18");
		 $(".gaz_rares").children(".element").css("background","rgba(64, 192, 0, 0.9) ");
		 $(".halogènes").children(".element").css("background"," #00E676 ");
		 $(".non-métaux").children(".element").css("background"," rgba(0, 192, 64, 0.6)");
		 $(".métalloides").children(".element").css("background"," rgba(0, 160, 96, 0.9) ");
		 $(".métaux_pauvres").children(".element").css("background","rgba(0, 96, 160, 0.6) ");

  

				  });


	 });


</script>


<style>
#popup_container{background-color:#f2f2f4; width:42%; height:165px; position:absolute; top:200px; left:300px; display:none}
.tempPopup_blk{background-color:#f2f2f4; width:35%; height:125px; position:absolute; top:200px; left:300px; display:none }
.element_header{ background-color:#40C4FF; height:40px; font-size:30px; padding-top:5px; padding-left:5px; color:#fff}
.element_hover_image { float:left}
.element_hover_details{ float:right; background-color:#40C4FF; margin: -106px 0px; width:120px; height:106px}
.element_hover_data{ margin-top:5px}

.element_symbol{ font-size:50px; font-weight:bold; text-align:center}
.symbol_name{ text-align:center}
#num_atomique{ float:left; margin-top:0px; background-color:rgba(0, 128, 180, 0.9); padding:10px 10px 3px 10px}
#mass_atomique{ float:right; padding:8px 3px}
.td_height{ padding-top::5px; padding-bottom:5px}

.filters{ float:left; margin-left:10px; margin-top:10px}
.filter-names{ float: left; position:relative; top:2px; font-size:20px ; ; color:#555}
#densite-filter,#reset-filter{ background-color:#4267b2; color:#fff; padding: 5px; cursor:pointer }

#temp-filter{}
#rang{ float:left}
#temp_plus, #temp-{ padding:5px 10px 5px 10px;background-color:#4267b2; color:#fff; width:10px;float:left; font-size:20px; font-weight:bold;
cursor:pointer}
.temp-degree{ float:left; margin:0 10px 0 10Px}
#classification-filter{}
.filter-element{ padding:5px; background-color:#4267b2;color:#fff;float:left; margin-left:5px; cursor:pointer}

#group-filter{ }
#period-filter{ margin-top:-30px  }
#famille-filter{}
.top_blk ul{ margin-left:0px; border-bottom:1px solid #999}
.top_blk ul li { list-style:none; display: inline-block; }
.solidimg,.unknwimg,.gasimg,.liquidimg{ width:30px; height:30px;}
.solidimg{ background-color:#B72326}
.liquidimg{ background-color:blue}
.gasimg{ background-color:green}
.unknwimg{ background-color:grey}

.text_title{ position:relative; top:-10px; margin-left:30px}
.btm_blk li { margin-left:30px; padding-top:20px; font-size:30px; font-weight:bold}

.navbar{ background-color:#007bff; height:40px; margin-top:-16px}
.navbar ul li { display:inline-block; list-style:none; margin-left:20px; margin-top:5px;}
.navbar a {  color:#fff ; text-decoration:none; }
.navbar a:hover  { color:#333}



</style>
</head>

<body>
<nav class="navbar"  >
<ul>
<?php if (isset($_SESSION['user_id'])){ ?>
<li><a href="logout.php">logout</a></li>
<?php } ?>

<?php if (!isset($_SESSION['user_id'])){ ?>
<li><a href="login.php">login</a></li>
<?php } ?>

<li><a href="discuter.php">contacter un  expert </a></li>
<?php if (isset($_SESSION['user_id'])){ ?>
<li  style=" float:right; margin-right:40px; color:#fff" >  
<ul>
<li><img  src="images/default-profile.png" width="40" height="40" style=" border-radius:50%; margin-top:-10px; margin-bottom:-10px" ></li>
<li><?php echo $_SESSION['username']; ?>  </li>


</ul>

</li>
<?php } ?>
</ul>
      </nav>

<header> 

<div class="filters" id="reset-filter">annuler les filtres</div>
<div class="filters" id="densite-filter"> densité</div>

<div class="filters"  id="temp-filter"> <span class="temp-degree filter-names"> Temerature 0k </span>
<div id="temp-">&minus;</div>
<input type="range" id="rang" value="0" min="0" max="6000">
<div id="temp_plus">+</div>
<span class="temp-degree filter-names"> 6000K </span>
</div>
<div  class="filters" id="classification-filter">
<span  class="filter-names">classification</span>
<div class="filter-element filter_metal" id="1">métal</div>
<div class="filter-element filter_metal " id="0">non métal</div>

</div>

<div class="filters"  id="group-filter">
<span  class="filter-names">groupes</span>

<div class="filter-element filter_g "  id="group_1">1</div>
<div class="filter-element filter_g" id="group_2">2</div>
<div  class="filter-element filter_g" id="group_3">3</div>
<div class="filter-element filter_g" id="group_4">4</div>
<div class="filter-element filter_g"  id="group_5">5</div>
<div class="filter-element filter_g"  id="group_6">6</div>
<div  class="filter-element filter_g" id="group_7">7</div>
<div  class="filter-element filter_g" id="group_8">8</div>
<div  class="filter-element filter_g" id="group_9">9</div>
<div  class="filter-element filter_g" id="group_10">10</div>
<div  class="filter-element filter_g" id="group_11">11</div>
<div  class="filter-element filter_g" id="group_12">12</div>
<div class="filter-element filter_g"  id="group_13">13</div>
<div  class="filter-element filter_g" id="group_14">14</div>
<div  class="filter-element filter_g" id="group_15">15</div>
<div  class="filter-element filter_g" id="group_16">16</div>
<div  class="filter-element filter_g"  id="group_17">17</div>
<div  class="filter-element filter_g" id="group_18">18</div>

</div>
<div class="filters"  id="period-filter">
<span  class="filter-names">périodes</span>

<div class="filter-element filter_p" id="period_1">1</div>
<div  class="filter-element filter_p" id="period_2">2</div>
<div  class="filter-element filter_p" id="period_3">3</div>
<div class="filter-element filter_p" id="period_4">4</div>
<div class="filter-element filter_p"  id="period_5">5</div>
<div  class="filter-element filter_p" id="period_6">6</div>
<div  class="filter-element filter_p " id="period_7">7</div>
<div class="filter-element filter_p"  id="period_8">8</div>
<div class="filter-element filter_p" id="period_9">9</div>

</div>


<div  class="filters"  id="famille-filter">
<span  class="filter-names">familles</span>

<div class="filter-element filter_f" id="métaux alcalins">métaux alcalins </div>
<div class="filter-element filter_f"  id="métaux alcalino-terreux">métaux alcalino-terreux</div>
<div class="filter-element filter_f" id="métaux de transitions">métaux de transitions</div>
<div  class="filter-element filter_f" id="métaux pauvres">métaux pauvres</div>
<div  class="filter-element filter_f" id="métalloides">métalloides</div>
<div  class="filter-element filter_f" id="non-métaux">non-métaux</div>
<div  class="filter-element filter_f" id="halogènes"> halogènes</div>
<div  class="filter-element filter_f" id="gaz rares">gaz rares</div>
<div  class="filter-element filter_f" id="lanthanides">lanthanides</div>
<div  class="filter-element filter_f" id="actinides">actinides</div>

</div>


</header>
<div class="group_blocks element_info group_blocks_Nitrogen" id="popup_container" >
                <div class="element_header" id="elementname" style="background-color:red" ><span id="nom">Nitrogen</span></div>
                <div class="fl element_details">
                    <div class="fl element_hover_image" >
                        <img id="murrayImgId" border="0"  style="height: 106px!important; width: 100px!important;" src="jjj" width="106" height="106">
                    </div>
                    <div class="fl element_hover_data" >
                        <table cellspacing="0" cellpadding="0" border="0" class="element_hover_table" >
                            <tbody>
                                <tr >
                                    <td class="tlbox_he tdfirst_he td_bottom_Border_Color td_lB_clr td_height" >
                                       configuration d'electron
                                    </td>
                                    <td class="trbox_he tdfirst_he td_bottom_Border_Color td_height" id="e_config" ><sup>14</sup>N</td>
                                </tr>
                                <tr>
                                    <td class="tlbox_he tlbox_even_he td_bottom_Border_Color td_lB_clr td_height" > états d'oxydation
                                    </td>
                                    <td class="trbox_he tlbox_even_he td_bottom_Border_Color td_height" id="etats_oxydation" >[He] 2s<sup>2</sup>2p<sup>3</sup></td>
                                </tr>
                                <tr>
                                    <td class="tlbox_he td_bottom_Border_Color td_lB_clr td_height" >
                                        Densité(g cm<sup>−3</sup>)
                                    </td>
                                    <td class="trbox_he td_bottom_Border_Color td_height" id="densite" >0.001145</td>
                                </tr>
                                <tr>
                                    <td class="tlbox_he tdlast_he td_lB_clr td_height" >
                                        1<sup>er</sup>énergie d'ionisation
                                    </td>
                                    <td class="trbox_he tdlast_he td_height" id="energie_ionisation">1402.328 kJ mol<sup>−1</sup></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="fl element_hover_details">
                        <div class="element_hover_details_2">
                            <img id="symbolImg" border="0" alt="Nitrogen Symbol (N)" style="display: none" src="" title="Nitrogen Symbol (N)">
                            <div id="symbole" class="element_symbol element_detail">N</div>
                            <div id="nom" class="symbol_name element_detail">Nitrogen</div>
                        </div>
                        <div class="element_hover_details_1 element_detail" id="num_atomique" >7</div>
                        <div class="element_hover_details_3 element_detail" id="mass_atomique" >14.007</div>
                    </div>
                </div>
            </div>
            
     <div class="tempPopup_blk">
                    <div class="top_blk">
                        <ul class="top_blk_ul">
                             <li class="text_title" style="margin-left:0px">Solid</li>
                            <li class="solidimg"></li>
                          <li class="text_title">Liquid</li>
                            <li class="liquidimg"></li>
                                <li class="text_title">Gas</li>
                            <li class="gasimg"></li>
                             <li class="text_title">inconnu</li>
                            <li class="unknwimg"></li>
                        </ul>
                        <ul>
                                            <div class="btm_blk">

                              <li class="text_title"> <span id="kelvin"> </span> <span>Kelvin </span>  </li>
                             <li class="text_title"><span id="Celsius"> </span> <span>°Celsius </span> </li>
                                   </div>
                        </ul>
                    </div>
                </div>
                
                       
<div class="periodic" style=" width:90%; height:550px">
  <div class="periodic-row" >
  <a href="elements?id=1" target="_blank">
    <div class="cell  real_cell non-métaux" >
      <div class="element"  id="element_1"  <?php echo set_attr($elements[1]); ?> class="groupe_1" >
        <div class="at_num">1</div>
        <div class="symbol">H</div>
        <div class="at_details">hydrogen<br />1.008</div>
      </div>
    </div>
    </a>
    <div class="cell  "></div>
    <div class="cell  "></div>
    <div class="cell  "></div>
    <div class="cell  "></div>
    <div class="cell  "></div>
    <div class="cell  "></div>
    <div class="cell  "></div>
    <div class="cell  "></div>
    <div class="cell  "></div>
    <div class="cell  "></div>
    <div class="cell  "></div>
    <div class="cell  "></div>
    <div class="cell  "></div>
    <div class="cell  "></div>
    <div class="cell  "></div>
    <div class="cell  "></div>
      <a href="elements?id=2" target="_blank">
    <div class="cell  real_cell gaz_rares">
      <div class="element" id="element_2"   <?php echo set_attr($elements[2]); ?>>
        <div class="at_num">2</div>
        <div class="symbol">He</div>
        <div class="at_details">helium<br />4.0026</div>
      </div>
    </div></a>
  </div>
  <div class="periodic-row">
    <a href="elements?id=3" target="_blank">
    <div class="cell  real_cell  métaux_alcalins ">
      <div class="element" id="element_3"   <?php echo set_attr($elements[3]); ?>>
        <div class="at_num">3</div>
        <div class="symbol">Li</div>
        <div class="at_details">lithium<br />6.94</div>
      </div>
    </div></a>
      <a href="elements?id=4" target="_blank">
    <div class="cell  real_cell métaux_alcalino-terreux">
      <div class="element" id="element_4" <?php echo set_attr($elements[4]); ?>>
        <div class="at_num">4</div>
        <div class="symbol">Be</div>
        <div class="at_details">beryllium<br />9.0122</div>
      </div>
    </div></a>
    <div class="cell  "></div>
    <div class="cell "></div>
    <div class="cell  "></div>
    <div class="cell "></div>
    <div class="cell  "></div>
    <div class="cell "></div>
    <div class="cell  "></div>
    <div class="cell  "></div>
    <div class="cell  "></div>
    <div class="cell  "></div>
      <a href="elements?id=5" target="_blank">
    <div class="cell  real_cell métalloides">
      <div class="element" id="element_5" <?php echo set_attr($elements[5]); ?>>
        <div class="at_num">5</div>
        <div class="symbol">B</div>
        <div class="at_details">boron<br />10.81</div>
      </div>
    </div></a>
      <a href="elements?id=6" target="_blank">
    <div class="cell  real_cell non-métaux">
      <div class="element" id="element_6" <?php echo set_attr($elements[6]); ?>>
        <div class="at_num">6</div>
        <div class="symbol">C</div>
        <div class="at_details">carbon<br />12.011</div>
      </div>
    </div></a>
      <a href="elements?id=7" target="_blank">
    <div class="cell  real_cell non-métaux">
      <div class="element" id="element_7" <?php echo set_attr($elements[7]); ?> >
        <div class="at_num">7</div>
        <div class="symbol">N</div>
        <div class="at_details">nidivogen<br />14.007</div>
      </div>
    </div></a>
      <a href="elements?id=8" target="_blank">
    <div class="cell  real_cell non-métaux">
      <div class="element" id="element_8" <?php echo set_attr($elements[8]); ?>>
        <div class="at_num">8</div>
        <div class="symbol">O</div>
        <div class="at_details">oxygen<br />15.999</div>
      </div>
    </div></a>
      <a href="elements?id=9" target="_blank">
    <div class="cell  real_cell halogènes">
      <div class="element" id="element_9" <?php echo set_attr($elements[9]); ?>>
        <div class="at_num">9</div>
        <div class="symbol">F</div>
        <div class="at_details">fluorine<br />18.998</div>
      </div>
    </div></a>
      <a href="elements?id=10" target="_blank">
    <div class="cell  real_cell gaz_rares">
      <div class="element" id="element_10" <?php echo set_attr($elements[10]); ?>>
        <div class="at_num">10</div>
        <div class="symbol">Ne</div>
        <div class="at_details">neon<br />20.180</div>
      </div>
    </div></a>
  </div>
  <div class="periodic-row">
    <a href="elements?id=11" target="_blank">
    <div class="cell  real_cell métaux_alcalins">
      <div class="element" id="element_11" <?php echo set_attr($elements[11]); ?>>
        <div class="at_num">11</div>
        <div class="symbol">Na</div>
        <div class="at_details">sodium<br />22.990</div>
      </div>
    </div></a>
      <a href="elements?id=12" target="_blank">
    <div class="cell  real_cell métaux_alcalino-terreux">
      <div class="element" id="element_12" <?php echo set_attr($elements[12]); ?>>
        <div class="at_num">12</div>
        <div class="symbol">Mg</div>
        <div class="at_details">magnesium<br />24.305</div>
      </div>
    </div></a>
    <div class="cell  "></div>
    <div class="cell  "></div>
    <div class="cell  "></div>
    <div class="cell  "></div>
    <div class="cell  "></div>
    <div class="cell  "></div>
    <div class="cell  "></div>
    <div class="cell  "></div>
    <div class="cell  "></div>
    <div class="cell  "></div>
      <a href="elements?id=13" target="_blank">
    <div class="cell  real_cell métaux_pauvres">
      <div class="element" id="element_13" <?php echo set_attr($elements[13]); ?>>
        <div class="at_num">13</div>
        <div class="symbol">Al</div>
        <div class="at_details">aluminum<br />26.982</div>
      </div>
    </div></a>
      <a href="elements?id=14" target="_blank">
    <div class="cell  real_cell métalloides">
      <div class="element" id="element_14" <?php echo set_attr($elements[14]); ?>>
        <div class="at_num">14</div>
        <div class="symbol">Si</div>
        <div class="at_details">silicon<br />28.085</div>
      </div>
    </div></a>
      <a href="elements?id=15" target="_blank">
    <div class="cell  real_cell non-métaux">
      <div class="element" id="element_15" <?php echo set_attr($elements[15]); ?>>
        <div class="at_num">15</div>
        <div class="symbol">P</div>
        <div class="at_details">phosphorus<br />30.974</div>
      </div>
    </div></a>
      <a href="elements?id=16" target="_blank">
    <div class="cell  real_cell non-métaux">
      <div class="element" id="element_16" <?php echo set_attr($elements[16]); ?>>
        <div class="at_num">16</div>
        <div class="symbol">S</div>
        <div class="at_details">sulfur<br />32.06</div>
      </div>
    </div></a>
      <a href="elements?id=17" target="_blank">
    <div class="cell  real_cell halogènes">
      <div class="element" id="element_17" <?php echo set_attr($elements[17]); ?>>
        <div class="at_num">17</div>
        <div class="symbol">Cl</div>
        <div class="at_details">chlorine<br />35.45</div>
      </div>
    </div></a>
      <a href="elements?id=18" target="_blank">
    <div class="cell  real_cell gaz_rares">
      <div class="element" id="element_18" <?php echo set_attr($elements[18]); ?>>
        <div class="at_num">18</div>
        <div class="symbol">Ar</div>
        <div class="at_details">argon<br />39.948</div>
      </div>
    </div></a>
  </div>
  <div class="periodic-row">
    <a href="elements?id=19" target="_blank">
    <div class="cell  real_cell métaux_alcalins">
      <div class="element" id="element_19" <?php echo set_attr($elements[19]); ?>>
        <div class="at_num">19</div>
        <div class="symbol">K</div>
        <div class="at_details">potassium<br />39.098</div>
      </div>
    </div></a>
      <a href="elements?id=20" target="_blank">
    <div class="cell  real_cell métaux_alcalino-terreux">
      <div class="element" id="element_20" <?php echo set_attr($elements[20]); ?>>
        <div class="at_num">20</div>
        <div class="symbol">Ca</div>
        <div class="at_details">calcium<br />40.078</div>
      </div>
    </div></a>
          <a href="elements?id=21" target="_blank">
    <div class="cell  real_cell">
      <div class="element" id="element_21" <?php echo set_attr($elements[21]); ?>>
        <div class="at_num">21</div>
        <div class="symbol">Sc</div>
        <div class="at_details">scandium<br />44.956</div>
      </div>
    </div></a>
          <a href="elements?id=22" target="_blank">
    <div class="cell  real_cell ">
      <div class="element" id="element_22" <?php echo set_attr($elements[22]); ?>>
        <div class="at_num">22</div>
        <div class="symbol">Ti</div>
        <div class="at_details">titanium<br />47.867</div>
      </div>
    </div></a>
          <a href="elements?id=23" target="_blank">
    <div class="cell  real_cell">
      <div class="element" id="element_23" <?php echo set_attr($elements[23]); ?>>
        <div class="at_num">23</div>
        <div class="symbol">V</div>
        <div class="at_details">vanadium<br />50.942</div>
      </div>
    </div></a>
          <a href="elements?id=24" target="_blank">
    <div class="cell  real_cell">
      <div class="element" id="element_24" <?php echo set_attr($elements[24]); ?>>
        <div class="at_num">24</div>
        <div class="symbol">Cr</div>
        <div class="at_details">chromium<br />51.996</div>
      </div>
    </div></a>
          <a href="elements?id=25" target="_blank">
    <div class="cell  real_cell">
      <div class="element" id="element_25" <?php echo set_attr($elements[25]); ?>>
        <div class="at_num">25</div>
        <div class="symbol">Mn</div>
        <div class="at_details">manganese<br />54.938</div>
      </div>
    </div></a>
          <a href="elements?id=26" target="_blank">
    <div class="cell  real_cell">
      <div class="element" id="element_26" <?php echo set_attr($elements[26]); ?>>
        <div class="at_num">26</div>
        <div class="symbol">Fe</div>
        <div class="at_details">iron<br />55.845</div>
      </div>
    </div></a>
          <a href="elements?id=27" target="_blank">
    <div class="cell  real_cell">
      <div class="element" id="element_27" <?php echo set_attr($elements[27]); ?>>
        <div class="at_num">27</div>
        <div class="symbol">Co</div>
        <div class="at_details">cobalt<br />58.933</div>
      </div>
    </div></a>
          <a href="elements?id=28" target="_blank">
    <div class="cell  real_cell">
      <div class="element" id="element_28" <?php echo set_attr($elements[28]); ?>>
        <div class="at_num">28</div>
        <div class="symbol">Ni</div>
        <div class="at_details">nickel<br />58.693</div>
      </div>
    </div></a>
          <a href="elements?id=29" target="_blank">
    <div class="cell  real_cell">
      <div class="element" id="element_29" <?php echo set_attr($elements[29]); ?>>
        <div class="at_num">29</div>
        <div class="symbol">Cu</div>
        <div class="at_details">copper<br />63.546</div>
      </div>
    </div></a>
          <a href="elements?id=30" target="_blank">
    <div class="cell  real_cell">
      <div class="element" id="element_30" <?php echo set_attr($elements[30]); ?>>
        <div class="at_num">30</div>
        <div class="symbol">Zn</div>
        <div class="at_details">zinc<br />65.38</div>
      </div>
    </div></a>
              <a href="elements?id=31" target="_blank">
    <div class="cell  real_cell métaux_pauvres">
      <div class="element" id="element_31" <?php echo set_attr($elements[31]); ?>>
        <div class="at_num">31</div>
        <div class="symbol">Ga</div>
        <div class="at_details">gallium<br />69.723</div>
      </div>
    </div></a>
              <a href="elements?id=32" target="_blank">
    <div class="cell  real_cell métalloides">
      <div class="element" id="element_32" <?php echo set_attr($elements[32]); ?>>
        <div class="at_num">32</div>
        <div class="symbol">Ge</div>
        <div class="at_details">germanium<br />72.63</div>
      </div>
    </div></a>
              <a href="elements?id=33" target="_blank">
    <div class="cell  real_cell métalloides">
      <div class="element" id="element_33" <?php echo set_attr($elements[33]); ?>>
        <div class="at_num">33</div>
        <div class="symbol">As</div>
        <div class="at_details">arsenic<br />74.922</div>
      </div>
    </div></a>
              <a href="elements?id=34" target="_blank">
    <div class="cell  real_cell non-métaux">
      <div class="element" id="element_34" <?php echo set_attr($elements[34]); ?>>
        <div class="at_num">34</div>
        <div class="symbol">Se</div>
        <div class="at_details">selenium<br />78.96</div>
      </div>
    </div></a>
              <a href="elements?id=35" target="_blank">
    <div class="cell  real_cell halogènes">
      <div class="element" id="element_35" <?php echo set_attr($elements[35]); ?>>
        <div class="at_num">35</div>
        <div class="symbol">Br</div>
        <div class="at_details">bromine<br />79.904</div>
      </div>
    </div></a>
              <a href="elements?id=36" target="_blank">
    <div class="cell  real_cell gaz_rares">
      <div class="element" id="element_36" <?php echo set_attr($elements[36]); ?>>
        <div class="at_num">36</div>
        <div class="symbol">Kr</div>
        <div class="at_details">krypton<br />83.798</div>
      </div>
    </div></a>
  </div>
  <div class="periodic-row">
            <a href="elements?id=37" target="_blank">
    <div class="cell  real_cell métaux_alcalins">
      <div class="element" id="element_37" <?php echo set_attr($elements[37]); ?>>
        <div class="at_num">37</div>
        <div class="symbol">Rb</div>
        <div class="at_details">rubidium<br />85.468</div>
      </div>
    </div></a>
              <a href="elements?id=38" target="_blank">
    <div class="cell  real_cell métaux_alcalino-terreux">
      <div class="element" id="element_38" <?php echo set_attr($elements[38]); ?>>
        <div class="at_num">38</div>
        <div class="symbol">Sr</div>
        <div class="at_details">sdivontium<br />87.62</div>
      </div>
    </div></a>
              <a href="elements?id=39" target="_blank">
    <div class="cell  real_cell">
      <div class="element" id="element_39" <?php echo set_attr($elements[39]); ?>>
        <div class="at_num">39</div>
        <div class="symbol">Y</div>
        <div class="at_details">ytdivium<br />88.906</div>
      </div>
    </div></a>
              <a href="elements?id=40" target="_blank">
    <div class="cell  real_cell">
      <div class="element" id="element_40" <?php echo set_attr($elements[40]); ?>>
        <div class="at_num">40</div>
        <div class="symbol">Zr</div>
        <div class="at_details">zirconium<br />91.224</div>
      </div>
    </div></a>
                  <a href="elements?id=41" target="_blank">
    <div class="cell  real_cell">
      <div class="element" id="element_41" <?php echo set_attr($elements[41]); ?>>
        <div class="at_num">41</div>
        <div class="symbol">Nb</div>
        <div class="at_details">niobium<br />92.906</div>
      </div>
    </div></a>
                  <a href="elements?id=42" target="_blank">
    <div class="cell  real_cell">
      <div class="element" id="element_42" <?php echo set_attr($elements[42]); ?>>
        <div class="at_num">42</div>
        <div class="symbol">Mo</div>
        <div class="at_details">molybdenum<br />95.96</div>
      </div>
    </div></a>
                  <a href="elements?id=43" target="_blank">

    <div class="cell  real_cell">
      <div class="element" id="element_43" <?php echo set_attr($elements[43]); ?>>
        <div class="at_num">43</div>
        <div class="symbol">Tc</div>
        <div class="at_details">technetium<br />[97.91]</div>
      </div>
    </div></a>
                  <a href="elements?id=44" target="_blank">
    <div class="cell  real_cell">
      <div class="element" id="element_44" <?php echo set_attr($elements[44]); ?>>
        <div class="at_num">44</div>
        <div class="symbol">Ru</div>
        <div class="at_details">ruthenium<br />101.07</div>
      </div>
    </div></a>
                  <a href="elements?id=45" target="_blank">
    <div class="cell  real_cell">
      <div class="element" id="element_45" <?php echo set_attr($elements[45]); ?>>
        <div class="at_num">45</div>
        <div class="symbol">Rh</div>
        <div class="at_details">rhodium<br />102.91</div>
      </div>
    </div></a>
                  <a href="elements?id=46" target="_blank">
    <div class="cell  real_cell">
      <div class="element" id="element_46" <?php echo set_attr($elements[46]); ?>>
        <div class="at_num">46</div>
        <div class="symbol">Pd</div>
        <div class="at_details">palladium<br />106.42</div>
      </div>
    </div></a>
                  <a href="elements?id=47" target="_blank">
    <div class="cell  real_cell">
      <div class="element" id="element_47" <?php echo set_attr($elements[47]); ?>>
        <div class="at_num">47</div>
        <div class="symbol">Ag</div>
        <div class="at_details">silver<br />107.87</div>
      </div>
    </div></a>
                  <a href="elements?id=48" target="_blank">
    <div class="cell  real_cell">
      <div class="element" id="element_48" <?php echo set_attr($elements[48]); ?>>
        <div class="at_num">48</div>
        <div class="symbol">Cd</div>
        <div class="at_details">cadmium<br />112.41</div>
      </div>
    </div></a>
                  <a href="elements?id=49" target="_blank">
    <div class="cell  real_cell métaux_pauvres">
      <div class="element" id="element_49" <?php echo set_attr($elements[49]); ?>>
        <div class="at_num">49</div>
        <div class="symbol">In</div>
        <div class="at_details">indium<br />114.82</div>
      </div>
    </div></a>
                  <a href="elements?id=50" target="_blank">
    <div class="cell  real_cell métaux_pauvres">
      <div class="element" id="element_50" <?php echo set_attr($elements[50]); ?>>
        <div class="at_num">50</div>
        <div class="symbol">Sn</div>
        <div class="at_details">tin<br />118.71</div>
      </div>
    </div></a>
                      <a href="elements?id=51" target="_blank">
    <div class="cell  real_cell métalloides">
      <div class="element" id="element_51" <?php echo set_attr($elements[51]); ?>>
        <div class="at_num">51</div>
        <div class="symbol">Sb</div>
        <div class="at_details">antimony<br />121.76</div>
      </div>
    </div></a>
                      <a href="elements?id=52" target="_blank">
    <div class="cell  real_cell métalloides">
      <div class="element" id="element_52" <?php echo set_attr($elements[52]); ?>>
        <div class="at_num">52</div>
        <div class="symbol">Te</div>
        <div class="at_details">tellurium<br />127.60</div>
      </div>
    </div></a>
                      <a href="elements?id=53" target="_blank">
    <div class="cell  real_cell halogènes">
      <div class="element" id="element_53" <?php echo set_attr($elements[53]); ?>>
        <div class="at_num">53</div>
        <div class="symbol">I</div>
        <div class="at_details">iodine<br />126.90</div>
      </div>
    </div></a>
                      <a href="elements?id=54" target="_blank">
    <div class="cell  real_cell gaz_rares">
      <div class="element" id="element_54" <?php echo set_attr($elements[54]); ?>>
        <div class="at_num">54</div>
        <div class="symbol">Xe</div>
        <div class="at_details">xenon<br />131.29</div>
      </div>
    </div></a>
  </div>
  <div class="periodic-row">
                    <a href="elements?id=55" target="_blank">
    <div class="cell  real_cell métaux_alcalins">
      <div class="element" id="element_55" <?php echo set_attr($elements[55]); ?>>
        <div class="at_num">55</div>
        <div class="symbol">Cs</div>
        <div class="at_details">cesium<br />132.91</div>
      </div>
    </div></a>
                      <a href="elements?id=56" target="_blank">
    <div class="cell  real_cell métaux_alcalino-terreux">
      <div class="element" id="element_56" <?php echo set_attr($elements[56]); ?>>
        <div class="at_num">56</div>
        <div class="symbol">Ba</div>
        <div class="at_details">barium<br />137.33</div>
      </div>
    </div></a>
    <div class="cell  "></div>
                      <a href="elements?id=72" target="_blank">
    <div class="cell  real_cell">
      <div class="element" id="element_72" <?php echo set_attr($elements[72]); ?>>
        <div class="at_num">72</div>
        <div class="symbol">Hf</div>
        <div class="at_details">hafnium<br />178.49</div>
      </div>
    </div></a>
                          <a href="elements?id=73" target="_blank">
    <div class="cell  real_cell">
      <div class="element" id="element_73" <?php echo set_attr($elements[73]); ?>>
        <div class="at_num">73</div>
        <div class="symbol">Ta</div>
        <div class="at_details">tantalum<br />180.95</div>
      </div>
    </div></a>
                          <a href="elements?id=74" target="_blank">
    <div class="cell  real_cell">
      <div class="element" id="element_74" <?php echo set_attr($elements[74]); ?>>
        <div class="at_num">74</div>
        <div class="symbol">W</div>
        <div class="at_details">tungsten<br />183.84</div>
      </div>
    </div></a>
                          <a href="elements?id=75" target="_blank">
    <div class="cell  real_cell ">
      <div class="element" id="element_75" <?php echo set_attr($elements[75]); ?>>
        <div class="at_num">75</div>
        <div class="symbol">Re</div>
        <div class="at_details">rhenium<br />186.21</div>
      </div>
    </div></a>
                          <a href="elements?id=76" target="_blank">
    <div class="cell  real_cell">
      <div class="element" id="element_76" <?php echo set_attr($elements[76]); ?>>
        <div class="at_num">76</div>
        <div class="symbol">Os</div>
        <div class="at_details">osmium<br />190.23</div>
      </div>
    </div></a>
                          <a href="elements?id=77" target="_blank">
    <div class="cell  real_cell">
      <div class="element" id="element_77" <?php echo set_attr($elements[77]); ?>>
        <div class="at_num">77</div>
        <div class="symbol">Ir</div>
        <div class="at_details">iridium<br />192.22</div>
      </div>
    </div></a>
                          <a href="elements?id=78" target="_blank">
    <div class="cell  real_cell">
      <div class="element" id="element_78" <?php echo set_attr($elements[78]); ?>>
        <div class="at_num">78</div>
        <div class="symbol">Pt</div>
        <div class="at_details">platinum<br />195.08</div>
      </div>
    </div></a>
                      <a href="elements?id=79" target="_blank">
    <div class="cell  real_cell">
      <div class="element" id="element_79" <?php echo set_attr($elements[79]); ?>>
        <div class="at_num">79</div>
        <div class="symbol">Au</div>
        <div class="at_details">gold<br />196.97</div>
      </div>
    </div></a>
                          <a href="elements?id=80" target="_blank">
    <div class="cell  real_cell">
      <div class="element" id="element_80" <?php echo set_attr($elements[80]); ?>>
        <div class="at_num">80</div>
        <div class="symbol">Hg</div>
        <div class="at_details">mercury<br />200.59</div>
      </div>
    </div></a>
                              <a href="elements?id=81" target="_blank">
    <div class="cell  real_cell métaux_pauvres">
      <div class="element" id="element_81" <?php echo set_attr($elements[81]); ?>>
        <div class="at_num">81</div>
        <div class="symbol">Tl</div>
        <div class="at_details">thallium<br />204.38</div>
      </div>
    </div></a>
                              <a href="elements?id=82" target="_blank">
    <div class="cell  real_cell métaux_pauvres">
      <div class="element" id="element_82" <?php echo set_attr($elements[82]); ?>>
        <div class="at_num" >82</div>
        <div class="symbol">Pb</div>
        <div class="at_details">lead<br />207.2</div>
      </div>
    </div></a>
                              <a href="elements?id=83" target="_blank">
    <div class="cell  real_cell métaux_pauvres">
      <div class="element" id="element_83" <?php echo set_attr($elements[83]); ?>>
        <div class="at_num">83</div>
        <div class="symbol">Bi</div>
        <div class="at_details">bismuth<br />208.98</div>
      </div>
    </div></a>
                              <a href="elements?id=84" target="_blank">
    <div class="cell  real_cell métalloides">
      <div class="element" id="element_84" <?php echo set_attr($elements[84]); ?>>
        <div class="at_num">84</div>
        <div class="symbol">Po</div>
        <div class="at_details">polonium<br />[208.98]</div>
      </div>
    </div></a>
                              <a href="elements?id=85" target="_blank">
    <div class="cell  real_cell halogènes">
      <div class="element" id="element_85" <?php echo set_attr($elements[85]); ?>>
        <div class="at_num">85</div>
        <div class="symbol">At</div>
        <div class="at_details">astatine<br />[209.99]</div>
      </div>
    </div></a>
                              <a href="elements?id=86" target="_blank">
    <div class="cell  real_cell gaz_rares">
      <div class="element" id="element_86" <?php echo set_attr($elements[86]); ?>>
        <div class="at_num">86</div>
        <div class="symbol">Rn</div>
        <div class="at_details">radon<br />[222.02]</div>
      </div>
    </div></a>
  </div>
  <div class="periodic-row">
                            <a href="elements?id=87" target="_blank">
    <div class="cell  real_cell métaux_alcalins">
      <div class="element" id="element_87" <?php echo set_attr($elements[87]); ?>>
        <div class="at_num">87</div>
        <div class="symbol">Fr</div>
        <div class="at_details">francium<br />[223.02]</div>
      </div>
    </div></a>
                              <a href="elements?id=88" target="_blank">
    <div class="cell  real_cell métaux_alcalino-terreux">
      <div class="element" id="element_88" <?php echo set_attr($elements[88]); ?>>
        <div class="at_num">88</div>
        <div class="symbol">Ra</div>
        <div class="at_details">radium<br />[226.03]</div>
      </div>
    </div></a>
    <div class="cell  "></div>
                              <a href="elements?id=104" target="_blank">
    <div class="cell  real_cell">
      <div class="element" id="element_104" <?php echo set_attr($elements[104]); ?>>
        <div class="at_num">104</div>
        <div class="symbol">Rf</div>
        <div class="at_details">rutherfordium<br />[265.12]</div>
      </div>
    </div></a>
                                  <a href="elements?id=105" target="_blank">
    <div class="cell  real_cell">
      <div class="element" id="element_105" <?php echo set_attr($elements[105]); ?>>
        <div class="at_num">105</div>
        <div class="symbol">Db</div>
        <div class="at_details">dubnium<br />[268.13]</div>
      </div>
    </div></a>
                                  <a href="elements?id=106" target="_blank">
    <div class="cell  real_cell">
      <div class="element" id="element_106" <?php echo set_attr($elements[106]); ?>>
        <div class="at_num">106</div>
        <div class="symbol">Sg</div>
        <div class="at_details">seaborgium<br />[271.13]</div>
      </div>
    </div></a>
                                  <a href="elements?id=107" target="_blank">
    <div class="cell  real_cell">
      <div class="element" id="element_107" <?php echo set_attr($elements[107]); ?>>
        <div class="at_num" >107</div>
        <div class="symbol">Bh</div>
        <div class="at_details">bohrium<br />[270]</div>
      </div>
    </div></a>
                                  <a href="elements?id=108" target="_blank">
    <div class="cell  real_cell">
      <div class="element" id="element_108" <?php echo set_attr($elements[108]); ?>>
        <div class="at_num">108</div>
        <div class="symbol">Hs</div>
        <div class="at_details">hassium<br />[277.15]</div>
      </div>
    </div></a>
                                  <a href="elements?id=109" target="_blank">
    <div class="cell  real_cell">
      <div class="element" id="element_109" <?php echo set_attr($elements[109]); ?>>
        <div class="at_num">109</div>
        <div class="symbol">Mt</div>
        <div class="at_details">meitnerium<br />[276.15]</div>
      </div>
    </div></a>
                                  <a href="elements?id=110" target="_blank">
    <div class="cell  real_cell">
      <div class="element" id="element_110" <?php echo set_attr($elements[110]); ?>>
        <div class="at_num">110</div>
        <div class="symbol">Ds</div>
        <div class="at_details">darmstadtium<br />[281.16]</div>
      </div>
    </div></a>
                                      <a href="elements?id=111" target="_blank">
    <div class="cell  real_cell">
      <div class="element" id="element_111" <?php echo set_attr($elements[111]); ?>>
        <div class="at_num">111</div>
        <div class="symbol">Rg</div>
        <div class="at_details">roentgenium<br />[280.16]</div>
      </div>
    </div></a>
                                      <a href="elements?id=112" target="_blank">
    <div class="cell  real_cell">
      <div class="element" id="element_112" <?php echo set_attr($elements[112]); ?>>
        <div class="at_num">112</div>
        <div class="symbol">Cn</div>
        <div class="at_details">copernicium<br />[285.17]</div>
      </div>
    </div></a>
                                      <a href="elements?id=113" target="_blank">
    <div class="cell  real_cell métaux_pauvres">
      <div class="element" id="element_113" <?php echo set_attr($elements[113]); ?>>
        <div class="at_num">113</div>
        <div class="symbol">Nh</div>
        <div class="at_details">Nihonuim<br />[284.18]</div>
      </div>
    </div></a>
                                      <a href="elements?id=114" target="_blank">
    <div class="cell  real_cell métaux_pauvres">
      <div class="element" id="element_114" <?php echo set_attr($elements[114]); ?>>
        <div class="at_num">114</div>
        <div class="symbol">Fl</div>
        <div class="at_details">flerovium<br />[289.19]</div>
      </div>
    </div></a>
                                      <a href="elements?id=115" target="_blank">
    <div class="cell  real_cell métaux_pauvres" >
      <div class="element" id="element_115" <?php echo set_attr($elements[115]); ?>>
        <div class="at_num">115</div>
        <div class="symbol">Mc</div>
        <div class="at_details">Moscovi<br />[288.19]</div>
      </div>
    </div></a>
                                      <a href="elements?id=116" target="_blank">
    <div class="cell  real_cell métaux_pauvres">
      <div class="element" id="element_116" <?php echo set_attr($elements[116]); ?>>
        <div class="at_num">116</div>
        <div class="symbol">Lv</div>
        <div class="at_details">livermorium<br />[293]</div>
      </div>
    </div></a>
                                      <a href="elements?id=117" target="_blank">
    <div class="cell  real_cell métalloides">
      <div class="element" id="element_117" <?php echo set_attr($elements[117]); ?>>
        <div class="at_num">117</div>
        <div class="symbol">Ts</div>
        <div class="at_details">tenness<br />[294]</div>
      </div>
    </div></a>
                                      <a href="elements?id=118" target="_blank">
    <div class="cell  real_cell gaz_rares">
      <div class="element" id="element_118" <?php echo set_attr($elements[118]); ?>>
        <div class="at_num">118</div>
        <div class="symbol">Og</div>
        <div class="at_details">Oganes<br />[294]</div>
      </div>
    </div></a>
  </div>
  <div class="periodic-row"></div>
  <div class="periodic-row">
    <div class="cell  "></div>
    <div class="cell  "></div>
    <div class="cell  "></div>
                                      <a href="elements?id=57" target="_blank">
    <div class="cell  real_cell lanthanides">
      <div class="element" id="element_57" <?php echo set_attr($elements[57]); ?>>
        <div class="at_num">57</div>
        <div class="symbol">La</div>
        <div class="at_details">lanthanum<br />138.91</div>
      </div>
    </div></a>
                                          <a href="elements?id=58" target="_blank">
    <div class="cell  real_cell lanthanides">
      <div class="element" id="element_58" <?php echo set_attr($elements[58]); ?>>
        <div class="at_num">58</div>
        <div class="symbol">Ce</div>
        <div class="at_details">cerium<br />140.12</div>
      </div>
    </div></a>
                                          <a href="elements?id=59" target="_blank">
    <div class="cell  real_cell lanthanides">
      <div class="element" id="element_59" <?php echo set_attr($elements[59]); ?>>
        <div class="at_num">59</div>
        <div class="symbol">Pr</div>
        <div class="at_details">praseodymium<br />140.91</div>
      </div>
    </div></a>
                                          <a href="elements?id=60" target="_blank">
    <div class="cell  real_cell lanthanides">
      <div class="element" id="element_60" <?php echo set_attr($elements[60]); ?>>
        <div class="at_num">60</div>
        <div class="symbol">Nd</div>
        <div class="at_details">neodymium<br />144.24</div>
      </div>
    </div></a>
                                              <a href="elements?id=61" target="_blank">
    <div class="cell  real_cell lanthanides">
      <div class="element" id="element_61" <?php echo set_attr($elements[61]); ?>>
        <div class="at_num">61</div>
        <div class="symbol">Pm</div>
        <div class="at_details">promethium<br />[144.91]</div>
      </div>
    </div></a>
                                              <a href="elements?id=62" target="_blank">
    <div class="cell  real_cell lanthanides">
      <div class="element" id="element_62" <?php echo set_attr($elements[62]); ?>>
        <div class="at_num">62</div>
        <div class="symbol">Sm</div>
        <div class="at_details">samarium<br />150.36</div>
      </div>
    </div></a>
                                              <a href="elements?id=63" target="_blank">
    <div class="cell  real_cell lanthanides">
      <div class="element" id="element_63" <?php echo set_attr($elements[63]); ?>>
        <div class="at_num">63</div>
        <div class="symbol">Eu</div>
        <div class="at_details">europium<br />151.96</div>
      </div>
    </div></a>
                                              <a href="elements?id=64" target="_blank">
    <div class="cell  real_cell lanthanides">
      <div class="element" id="element_64" <?php echo set_attr($elements[64]); ?>>
        <div class="at_num">64</div>
        <div class="symbol">Gd</div>
        <div class="at_details">gadolinium<br />157.25</div>
      </div>
    </div></a>
                                              <a href="elements?id=65" target="_blank">
    <div class="cell  real_cell lanthanides">
      <div class="element" id="element_65" <?php echo set_attr($elements[65]); ?>>
        <div class="at_num">65</div>
        <div class="symbol">Tb</div>
        <div class="at_details">terbium<br />158.93</div>
      </div>
    </div></a>
                                              <a href="elements?id=66" target="_blank">
    <div class="cell  real_cell lanthanides">
      <div class="element" id="element_66" <?php echo set_attr($elements[66]); ?>>
        <div class="at_num">66</div>
        <div class="symbol">Dy</div>
        <div class="at_details">dysprosium<br />162.50</div>
      </div>
    </div></a>
                                              <a href="elements?id=67" target="_blank">
    <div class="cell  real_cell  lanthanides">
      <div class="element" id="element_67" <?php echo set_attr($elements[67]); ?>>
        <div class="at_num">67</div>
        <div class="symbol">Ho</div>
        <div class="at_details">holmium<br />164.93</div>
      </div>
    </div></a>
                                              <a href="elements?id=68" target="_blank">
    <div class="cell  real_cell lanthanides">
      <div class="element" id="element_68" <?php echo set_attr($elements[68]); ?>>
        <div class="at_num">68</div>
        <div class="symbol">Er</div>
        <div class="at_details">erbium<br />167.26</div>
      </div>
    </div></a>
                                              <a href="elements?id=69" target="_blank">
    <div class="cell  real_cell lanthanides">
      <div class="element" id="element_69" <?php echo set_attr($elements[69]); ?>>
        <div class="at_num">69</div>
        <div class="symbol">Tm</div>
        <div class="at_details">thulium<br />168.93</div>
      </div>
    </div></a>
                                              <a href="elements?id=70" target="_blank">
    <div class="cell  real_cell lanthanides">
      <div class="element" id="element_70" <?php echo set_attr($elements[70]); ?>>
        <div class="at_num">70</div>
        <div class="symbol">Yb</div>
        <div class="at_details">ytterbium<br />173.05</div>
      </div>
    </div></a>
                                              <a href="elements?id=71" target="_blank">
    <div class="cell  real_cell lanthanides">
      <div class="element" id="element_71" <?php echo set_attr($elements[71]); ?>>
        <div class="at_num">71</div>
        <div class="symbol">Lu</div>
        <div class="at_details">lutetium<br />174.97</div>
      </div>
    </div></a>
  </div>
  <div class="periodic-row">
    <div class="cell  "></div>
    <div class="cell  "></div>
    <div class="cell  "></div>
                                              <a href="elements?id=89" target="_blank">
    <div class="cell  real_cell actinides">
      <div class="element" id="element_89" <?php echo set_attr($elements[89]); ?>>
        <div class="at_num">89</div>
        <div class="symbol">Ac</div>
        <div class="at_details">actinium<br />[227.03]</div>
      </div>
    </div></a>
                                              <a href="elements?id=90" target="_blank">
    <div class="cell  real_cell actinides">
      <div class="element" id="element_90" <?php echo set_attr($elements[90]); ?>>
        <div class="at_num">90</div>
        <div class="symbol">Th</div>
        <div class="at_details">thorium<br />232.04</div>
      </div>
    </div></a>
                                                  <a href="elements?id=91" target="_blank">
    <div class="cell  real_cell actinides">
      <div class="element" id="element_91" <?php echo set_attr($elements[91]); ?>>
        <div class="at_num">91</div>
        <div class="symbol">Pa</div>
        <div class="at_details">protactinium<br />231.04</div>
      </div>
    </div></a>
                                                  <a href="elements?id=92" target="_blank">
    <div class="cell  real_cell actinides">
      <div class="element" id="element_92" <?php echo set_attr($elements[92]); ?>>
        <div class="at_num">92</div>
        <div class="symbol">U</div>
        <div class="at_details">uranium<br />238.03</div>
      </div>
    </div></a>
                                                  <a href="elements?id=93" target="_blank">
    <div class="cell  real_cell actinides">
      <div class="element" id="element_93" <?php echo set_attr($elements[93]); ?>>
        <div class="at_num">93</div>
        <div class="symbol">Np</div>
        <div class="at_details">neptunium<br />[237.05]</div>
      </div>
    </div></a>
                                                  <a href="elements?id=94" target="_blank">
    <div class="cell  real_cell actinides">
      <div class="element" id="element_94" <?php echo set_attr($elements[94]); ?>>
        <div class="at_num">94</div>
        <div class="symbol">Pu</div>
        <div class="at_details">plutonium<br />[244.06]</div>
      </div>
    </div></a>
                                                  <a href="elements?id=95" target="_blank">
    <div class="cell  real_cell actinides">
      <div class="element" id="element_95" <?php echo set_attr($elements[95]); ?>>
        <div class="at_num">95</div>
        <div class="symbol">Am</div>
        <div class="at_details">americium<br />[243.06]</div>
      </div>
    </div></a>
                                                  <a href="elements?id=96" target="_blank">
    <div class="cell  real_cell actinides">
      <div class="element" id=
      "element_96" <?php echo set_attr($elements[96]); ?>>
        <div class="at_num">96</div>
        <div class="symbol">Cm</div>
        <div class="at_details">curium<br />[247.07]</div>
      </div>
    </div></a>
                                                  <a href="elements?id=97" target="_blank">
    <div class="cell  real_cell actinides">
      <div class="element" id="element_97" <?php echo set_attr($elements[97]); ?>>
        <div class="at_num">97</div>
        <div class="symbol">Bk</div>
        <div class="at_details">berkelium<br />[247.07]</div>
      </div>
    </div></a>
                                                  <a href="elements?id=98" target="_blank">
    <div class="cell  real_cell actinides">
      <div class="element" id="element_98" <?php echo set_attr($elements[98]); ?>>
        <div class="at_num">98</div>
        <div class="symbol">Cf</div>
        <div class="at_details">californium<br />[251.08]</div>
      </div>
    </div></a>
                                                  <a href="elements?id=99" target="_blank">
    <div class="cell  real_cell actinides">
      <div class="element" id="element_99" <?php echo set_attr($elements[99]); ?>>
        <div class="at_num">99</div>
        <div class="symbol">Es</div>
        <div class="at_details">einsteinium<br />[252.08]</div>
      </div>
    </div></a>
                                                  <a href="elements?id=100" target="_blank">
    <div class="cell  real_cell actinides">
      <div class="element" id="element_100" <?php echo set_attr($elements[100]); ?>>
        <div class="at_num">100</div>
        <div class="symbol">Fm</div>
        <div class="at_details">fermium<br />[257.10]</div>
      </div>
    </div></a>
                                                      <a href="elements?id=101" target="_blank">
    <div class="cell  real_cell actinides">
      <div class="element" id="element_101" <?php echo set_attr($elements[101]); ?>>
        <div class="at_num">101</div>
        <div class="symbol">Md</div>
        <div class="at_details">mendelevium<br />[258.10]</div>
      </div>
    </div></a>
                                                      <a href="elements?id=102" target="_blank">
    <div class="cell  real_cell actinides">
      <div class="element" id="element_102" <?php echo set_attr($elements[102]); ?>>
        <div class="at_num">102</div>
        <div class="symbol">No</div>
        <div class="at_details">nobelium<br />[259.10]</div>
      </div>
    </div></a>
                                                      <a href="elements?id=103" target="_blank">
    <div class="cell  real_cell actinides">
      <div class="element" id="element_103" <?php echo set_attr($elements[103]); ?>>
        <div class="at_num">103</div>
        <div class="symbol">Lr</div>
        <div class="at_details">lawrencium<br />[262.11]</div>
      </div>
    </div></a>
  </div>
  <div style="clear: both;"></div>
</div>
</body>
</html>
