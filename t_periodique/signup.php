<?php
error_reporting(0); 
require 'functions/functions.php';
session_start();

if (!isset($_SESSION['is_admin']) OR !$_SESSION['is_admin']) {
    header("location:home.php");
}
ob_start(); 
?>
<!DOCTYPE html>
<html>
<head>
    <title>Social Network</title>
    <link rel="stylesheet" type="text/css" href="src/main.css?j=1">
    <style>
        .container{
            margin: 40px auto;
            width: 400px;
        }
        .content {
            padding: 30px;
            background-color: white;
            box-shadow: 0 0 5px #4267b2;
        }
    </style>
</head>
<body>
<br><br>
    <div class="container">
        <div class="tab">
            <button class="tablink" onclick="openTab(event,'signup')" id="link2" style="width:400px">Créer Un nouveau compt </button>
        </div>
        <div class="content">
            <div class="tabcontent" id="signup" style=" display:block">
                <form method="post" onsubmit="return validateRegister()">
                    <!--Package One-->
                    <hr>
                    <!--First Name-->
                    <label>Nom<span>*</span></label><br>
                    <input type="text" name="userfirstname" id="userfirstname">
                    <div class="required"></div>
                    <br>
                    <!--Last Name-->
                    <label>Prénom<span>*</span></label><br>
                    <input type="text" name="userlastname" id="userlastname">
                    <div class="required"></div>
                    <br>
                    <!--Nickname-->
                   
                    <!--Password-->
                    <label>Mot de passe <span>*</span></label><br>
                    <input type="password" name="userpass" id="userpass">
                    <div class="required"></div>
                    <br>
                    <!--Confirm Password-->
                    <label>Confirmer le mot de passe<span>*</span></label><br>
                    <input type="password" name="userpassconfirm" id="userpassconfirm">
                    <div class="required"></div>
                    <br>
                    <!--Email-->
                    <label>Email<span>*</span></label><br>
                    <input type="text" name="useremail" id="useremail">
                    <div class="required"></div>
                    <br>
                    <!--Birth Date-->
                   Date de naissance<span>*</span><br>
                    <select name="selectday">
                    <?php
                    for($i=1; $i<=31; $i++){
                        echo '<option value="'. $i .'">'. $i .'</option>';
                    }
                    ?>
                    </select>
                    <select name="selectmonth">
                    <?php
                    echo '<option value="1">January</option>';
                    echo '<option value="2">February</option>';
                    echo '<option value="3">March</option>';
                    echo '<option value="4">April</option>';
                    echo '<option value="5">May</option>';
                    echo '<option value="6">June</option>';
                    echo '<option value="7">July</option>';
                    echo '<option value="8">August</option>';
                    echo '<option value="9">September</option>';
                    echo '<option value="10">October</option>';
                    echo '<option value="11">Novemeber</option>';
                    echo '<option value="12">December</option>';
                    ?>
                    </select>
                    <select name="selectyear">
                    <?php
                    for($i=2017; $i>=1900; $i--){
                        if($i == 1996){
                            echo '<option value="'. $i .'" selected>'. $i .'</option>';
                        }
                        echo '<option value="'. $i .'">'. $i .'</option>';
                    }
                    ?>
                    </select>
                    <br><br>
                    <!--Gender-->
                     Groupe <span>*</span><br>
                    <select name="usergroup">
                    <option value="1">première année Licence (L1)</option>
                    <option value="2">deusième année Licence (L2)</option>
                    <option value="3">troisième année Licence (L3)</option>
                    <option value="4">première année Master (M1)</option>
                    <option value="5">deusième année Master (M2)</option>

                     </select>
                    <!--Hometown-->
                  <br><br>
                    <!--Package Two-->
                    <hr>
                    <!--Marital Status-->
                  
                    <input type="submit" value="Créer" name="register">
                </form>
            </div>
        </div>
    </div>
    <script src="src/main.js"></script>
</body>
</html>

<?php
$conn = connect();
if ($_SERVER['REQUEST_METHOD'] == 'POST') { // A form is posted
    if (isset($_POST['register'])) { // Register process
        // Retrieve Data
        $userfirstname = $_POST['userfirstname'];
        $userlastname = $_POST['userlastname'];
        $userpassword = md5($_POST['userpass']);
        $useremail = $_POST['useremail'];
        $userbirthdate = $_POST['selectyear'] . '-' . $_POST['selectmonth'] . '-' . $_POST['selectday'];
        $usergroup = $_POST['usergroup'];

        /* Check for Some Unique Constraints
        $query = mysqli_query($conn, "SELECT  email FROM users WHERE  email = '$useremail'");
        if(mysqli_num_rows($query) > 0){
            $row = mysqli_fetch_assoc($query);
            
            if($useremail == $row['email']){
                ?> <script>
                document.getElementsByClassName("required")[7].innerHTML = "This Email already exists.";
                </script> <?php
            }
        }
		*/
        // Insert Data
        $sql = "INSERT INTO users(nom, prenom, password,b_day, email,groupe_id)
                VALUES ('$userfirstname', '$userlastname', '$userpassword', '$userbirthdate', '$useremail', '$usergroup')";
        $query = mysqli_query($conn, $sql);
    }
}
?>
